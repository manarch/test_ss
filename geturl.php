<?php

include 'Database.php';

if( !isset($_GET['url']) )
    die();

$url = $_GET['url'];

$db = new Database;
$db->open();

while( ($result = $db->searchUrl( $url )) === false )
{
    $db->generateUrl( $url );
}

$db->close();

echo $result;
