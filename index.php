<?php
    if ( $_SERVER['REQUEST_URI'] != '/' ) {
        include 'Database.php';

        $db = new Database;
        $db->open();

        $alias = substr($_SERVER['REQUEST_URI'], 1);
        $result = $db->searchAlias( $alias );
        $db->close();

        if ( $result !== false ){
            if( !preg_match('/^\w+:\/\//', $result) )
                $result = "http://".$result;
            header("Location: ".$result);
        }else{
            header("HTTP/1.0 404 Not Found");
        }
    } else {
?>
<!DOCTYPE html>
<html lang="ru">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Тестовое задание Simetricsoft</title>
  <link rel="stylesheet" type="text/css" media="screen" href="/css/bootstrap.min.css">
  <style type="text/css">
  body{ padding: 20px; }
  .lturl{ padding-top: 30px; }
  </style>
</head>
<body>
  <div class="container">
    <form action="geturl.php">
      <div class="form-group">
        <label for="url">URL</label>
        <input type="text" class="form-control" id="url" name="url" aria-describedby="urlHelp" placeholder="Введите URL">
        <small id="urlHelp" class="form-text text-muted">Введите уникальный адрес.</small>
      </div>
      <button type="submit" class="btn btn-primary">Отправить запрос</button>
    </form>
    <div class="form-group lturl">
      <label for="resultUrl">Короткий URL</label>
      <input type="text" class="form-control" id="resultUrl" readonly>
    </div>
    <a href="/task2.php">Переход к заданию 2</a>
  </div>
  <script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="/js/test_ss.js"></script>
</body>
</html>

<?php } ?>
