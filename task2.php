<?php
  include 'calcCutView.php';
  $points = [[3,4],[6,8],[9,4]];
  $result = calcCutView($points);
?>
<!DOCTYPE html>
<html lang="ru">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Тестовое задание Simetricsoft</title>
  <link rel="stylesheet" type="text/css" media="screen" href="/css/bootstrap.min.css">
  <style type="text/css">
  body{ padding: 20px; }
  form{ padding-bottom: 20px;  }
  #svgplot{ height: 480px; width: 100%; border: 1px solid black; }
  .col{ max-width: 200px; }
  </style>
  <script type="text/javascript">
  var points = <?php echo json_encode($result); ?>;
  </script>
</head>
<body>
  <div class="container">
    <form action="getpoints.php">
      <div class="form-row">
        <div class="col">
          <label for="point_0_0">A(x)</label>
          <input type="number" class="form-control" id="point_0_0" name="point[0][0]" value="<?php echo $result[0][1][0]; ?>">
        </div>
        <div class="col">
          <label for="point_0_1">A(y)</label>
          <input type="number" class="form-control" id="point_0_1" name="point[0][1]" value="<?php echo $result[0][1][1]; ?>">
        </div>
      </div>
      <div class="form-row">
        <div class="col">
          <label for="point_0_0">B(x)</label>
          <input type="number" class="form-control" id="point_0_0" name="point[1][0]" value="<?php echo $result[1][1][0]; ?>">
        </div>
        <div class="col">
          <label for="point_0_1">B(y)</label>
          <input type="number" class="form-control" id="point_0_1" name="point[1][1]" value="<?php echo $result[1][1][1]; ?>">
        </div>
      </div>
      <div class="form-group form-row">
        <div class="col">
          <label for="point_0_0">C(x)</label>
          <input type="number" class="form-control" id="point_0_0" name="point[2][0]" value="<?php echo $result[2][1][0]; ?>">
        </div>
        <div class="col">
          <label for="point_0_1">C(y)</label>
          <input type="number" class="form-control" id="point_0_1" name="point[2][1]" value="<?php echo $result[2][1][1]; ?>">
        </div>
      </div>
      <button type="submit" class="btn btn-primary">Отправить запрос</button>
    </form>
    <div id="svgplot"></div>
    <a href="/">Переход к заданию 1</a>
  </div>
  <script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="/js/jquery.svg.min.js"></script>
  <script type="text/javascript" src="/js/test_ss_task2.js"></script>
</body>
</html>
