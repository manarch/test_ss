
$(document).ready(function()
{
    $('form').submit(function()
    {
        var self = $(this);
        $('#resultUrl').val('');
        $.ajax({
            url: self.attr('action'),
            type: "GET",
            data: self.serializeArray(),
            success: function( url ){
                $('#resultUrl').val(url);
            },
        });
        return false;
    });
});
