$('document').ready(function(){

    function resetSize(svg, width, height) {
        svg.configure({width: width || $(svg._container).width(),
            height: height || $(svg._container).height()});
    }
    $('#svgplot').svg(initPlot);

    function initPlot(svg) {
        var min = [], max = [];
        for( i in points ){
            if( Number(i) != i )
                continue;
            for( j in points[i] ){
                if (typeof min[0] === "undefined" ) {
                    min[0] = points[i][j][0];
                }else{
                    min[0] = Math.min(min[0], points[i][j][0]);
                }
                if (typeof min[1] === "undefined" ) {
                    min[1] = points[i][j][1];
                }else{
                    min[1] = Math.min(min[1], points[i][j][1]);
                }
                if (typeof max[0] === "undefined" ) {
                    max[0] = points[i][j][0];
                }else{
                    max[0] = Math.max(max[0], points[i][j][0]);
                }
                if (typeof max[1] === "undefined" ) {
                    max[1] = points[i][j][1];
                }else{
                    max[1] = Math.max(max[1], points[i][j][1]);
                }
            }
        }
        var height = svg.height(),
            width = svg.width(),
            scale = Math.min( (width - 40 ) / Math.abs(max[0] - min[0] ), (height - 40 ) / Math.abs(max[1] - min[1] ) ),
            radius = [ 3, 5, 3],
            text = ['A','B', 'C'],
            g = svg.group();

        for( i in points ){
            if( Number(i) != i )
                continue;
            var p = [];
            for( j in points[i] ){
                var cx = 20 + (points[i][j][0] - min[0] ) * scale;
                var cy = height - (20 + (points[i][j][1] - min[1]) * scale);
                svg.circle(g, cx, cy, radius[j], {fill: 'black', stroke: 'black', strokeWidth: 1} );
                svg.text(g, cx+5, cy+5, (j!=1 ? text[i].toLowerCase()+(j==0?'1':j) : text[i]) );
                p.push([cx,cy]);
            }
            svg.line(g, p[1][0], p[1][1], p[0][0], p[0][1], {fill: 'black', stroke: 'black', strokeWidth: 1} );
            svg.line(g, p[1][0], p[1][1], p[2][0], p[2][1], {fill: 'black', stroke: 'black', strokeWidth: 1} );
        }
    }

    $('form').submit(function()
    {
        var self = $(this);
        $.ajax({
            url: self.attr('action'),
            type: "GET",
            data: self.serializeArray(),
            success: function( data ){
                points = data;
                $('#svgplot').svg('destroy');
                $('#svgplot').svg(initPlot);
            },
            dataType: 'JSON'
        });
        return false;
    });


});
