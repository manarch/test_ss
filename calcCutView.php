<?php

// points: [[xA,yA],[xB,yB],[xC,yC]]
function calcCutView( $poins )
{
    if( count($poins)<3 )
        return false;
    $lengthAB = sqrt(pow($poins[0][0]-$poins[1][0], 2)+pow($poins[0][1]-$poins[1][1], 2));
    $lengthBC = sqrt(pow($poins[1][0]-$poins[2][0], 2)+pow($poins[1][1]-$poins[2][1], 2));

    $length = min($lengthAB, $lengthBC) * 0.25;

    $result = [
        [$poins[0],$poins[0],$poins[0]],
        [$poins[1],$poins[1],$poins[1]],
        [$poins[2],$poins[2],$poins[2]],
    ];

    $result[0][0][1] = $poins[0][1] > $poins[1][1] ? $poins[0][1] + $length : $poins[0][1] - $length; // A1[y]
    $result[2][2][1] = $poins[2][1] > $poins[1][1] ? $poins[2][1] - $length : $poins[2][1] + $length; // C1[y]

    $deltaAB = $length/$lengthAB;
    $deltaBC = $length/$lengthBC;

    $lineXAB = (($poins[0][0] - $poins[1][0]) * $deltaAB );
    $lineYAB = (($poins[0][1] - $poins[1][1]) * $deltaAB ) ;

    $result[0][2][0] = $poins[0][0] - $lineXAB ; // A2[x]
    $result[0][2][1] = $poins[0][1] - $lineYAB ; // A2[Y]

    $result[1][0][0] = $poins[1][0] + $lineXAB ; // B1[x]
    $result[1][0][1] = $poins[1][1] + $lineYAB ; // B1[Y]

    $lineXBC = (($poins[1][0] - $poins[2][0]) * $deltaBC );
    $lineYBC = (($poins[1][1] - $poins[2][1]) * $deltaBC ) ;

    $result[1][2][0] = $poins[1][0] - $lineXBC ; // B2[x]
    $result[1][2][1] = $poins[1][1] - $lineYBC ; // B2[Y]

    $result[2][0][0] = $poins[2][0] + $lineXBC ; // C1[x]
    $result[2][0][1] = $poins[2][1] + $lineYBC ; // C1[Y]

    return $result;
}
