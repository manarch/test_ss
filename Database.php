<?php

class Database
{
    private $connection = null;
    private $DBHOST = "localhost";
    private $DBNAME = "test-ss";
    private $DBUSER = "root";
    private $DBPASS = "h7!nIay6HUtu";

    // открыть соединение с БД
    public function open()
    {
        try {
            $this->connection = new PDO("mysql:host=".$this->DBHOST.";dbname=".$this->DBNAME, $this->DBUSER, $this->DBPASS);
        }
        catch(PDOException $e) {
            $this->connection = null;
        }
        return $this->connection !== null;
    }

    // поиск alias по заданному URL
    public function searchUrl( $url )
    {
        if( !$this->connection )
            return false;
        $query = "SELECT * FROM urlalias WHERE url=".$this->connection->quote($url)." LIMIT 1";
        $result = $this->connection->query($query, PDO::FETCH_ASSOC);
        if ( $result === false )
            return false;
        $data =  $result->fetchAll();
        if ( !empty($data) ) {
            return $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/'.$data[0]['alias'];
        }
        return false;
    }

    // генерация alias по заданному URL
    public function generateUrl( $url )
    {
        if( !$this->connection )
            return false;
        // цикл для формирования уникального alias
        for( $i=0; $i<10; $i++){
            // для генерации alias используем хэш-функцию SHA1.
            // из полученного значения удаляем впереди стоящие цифры и ограничиваем длинну alias 8 символами
            $alias = substr(preg_replace('/^\d+/', '', sha1($url.($i?$i:''), false)), 0, 8);
            // функция проверки уникальности делегируем в БД за счет задания уникальности поля 'alias'
            $query = $this->connection->prepare("INSERT INTO urlalias(url, alias) VALUES(:url, :alias);");
            $result = $query->execute( array('url'=>$url, 'alias'=>$alias) );
            if( $result !== false ) // запись прошла успешно, значить alias - уникальный, иначе генерируем новый
                break;
        }
        return true;
    }

    // поиск URL по заданному alias
    public function searchAlias( $alias )
    {
        if( !$this->connection )
            return false;
        $query = "SELECT * FROM urlalias WHERE alias=".$this->connection->quote($alias)." LIMIT 1";
        $result = $this->connection->query($query, PDO::FETCH_ASSOC);
        if ( $result === false )
            return false;
        $data =  $result->fetchAll();
        if ( !empty($data) ) {
            return $data[0]['url'];
        }
        return false;
    }
    public function close()
    {
        unset($this->connection);
    }

}
